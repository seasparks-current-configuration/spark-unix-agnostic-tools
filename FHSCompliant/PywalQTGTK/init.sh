#!/bin/sh

# Find script directory
directoryx="$(dirname -- $(readlink -fn -- "$0"; echo x))"
directory="${directoryx%x}"

# Wallpapers
mkdir -p ~/.config/wallpapers/
cp $directory/wallpapers/* ~/.config/wallpapers/

# Pywal Templates
mkdir -p ~/.config/wal/templates/
cp $directory/templates/* ~/.config/wal/templates/

# Call wal to generate templates
wal -i ~/.config/wallpapers/

# Setup QT theme
mkdir -p ~/.config/qt5ct/colors
ln -sf ~/.cache/wal/QT5CTPywalDark.conf  ~/.config/qt5ct/colors/QT5CTPywalDark.conf 

# Setup rofi theme
mkdir -p ~/.config/rofi
ln -sf ~/.cache/wal/RofiPywal.rasi ~/.config/rofi/colors.rasi

# Setup mako theme
mkdir -p ~/.config/mako
ln -sf ~/.cache/wal/MakoPywal.conf ~/.config/mako/config

# Setup GTK/Icon theme
mkdir -p ~/.icons
mkdir -p ~/.themes
wpg-install.sh -gi

# Check if dolphin exists, if it does then create walphin entry. Important to create this to hardlink path, XDG MIME does not like environment variables in the exec field.
if [ -f /usr/local/bin/dolphin ]; then
    mkdir -p ${HOME}/.local/share/applications
    tee ${HOME}/.local/share/applications/walphin.desktop <<EOF
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Walphin
Comment=The Dolphin File Manager configured for use with QT5CT/Pywal
Exec=dolphin --stylesheet=/home/${USER}/.config/qt5ct/qss/dolphin-background-fix.qss
Icon=application.png
Categories=Network;WebBrowser;
GenericName=The Dolphin File Manager configured for use with QT5CT/Pywal
Terminal=false
EOF
fi
