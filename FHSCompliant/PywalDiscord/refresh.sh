#!/bin/sh

# Ensure wal cache and betterdiscord directory exists
mkdir -p ~/.cache/wal/
mkdir -p ~/.config/BetterDiscord/themes/

# Make css file and output to betterdiscord
cat ~/.config/spark-unix-agnostic-tools/FHSCompliant/PywalDiscord/meta.css ~/.cache/wal/colors.css ~/.config/spark-unix-agnostic-tools/FHSCompliant/PywalDiscord/PywalDiscord.theme.css > ~/.config/BetterDiscord/themes/PywalDiscord.theme.css
