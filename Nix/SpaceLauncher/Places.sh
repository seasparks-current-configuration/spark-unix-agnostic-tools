#!/run/current-system/sw/bin/bash

MEDIADIR=/run/media/$USER
FILEMANAGER="dolphin --stylesheet=$HOME/.config/qt5ct/qss/dolphin-background-fix.qss"

if [ -z $@ ]; then
    echo "______________________________________________________________________________________"
    # Places to check.
    # Get only directories.
    tree -i --noreport -d -L 1 $HOME | tail -n +2
    tree -i --noreport -d -L 1 $MEDIADIR | tail -n +2
else
    # Check which directory the place is in (Home or Media), you can add more here.
    # Do an explicit redirection of the standard output and errors streams meaning that the user will not be prompted for input by nohup and the script will consequently continue.
    if [ -d $HOME/$@ ]; then
	nohup $FILEMANAGER $HOME/$@ >> nohup.out 2>&1 &
    elif [ -d $MEDIADIR/$@ ]; then
	nohup $FILEMANAGER $MEDIADIR/$@ >> nohup.out 2>&1 &
    fi;
    exit 0
fi
