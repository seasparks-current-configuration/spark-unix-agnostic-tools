#!/run/current-system/sw/bin/sh

# Select random wallpaper in directory and store it in variable
WALLPAPER=`find ${HOME}/.config/wallpapers/ -type f | shuf -n 1 | rev | cut -d '/' -f 1 | rev`

# Set pywal templates/QT/GTK theme
wpg -s "${HOME}/.config/wallpapers/${WALLPAPER}"

# Light mode
# wpg -Ls "${HOME}/.config/wallpapers/${WALLPAPER}"

# Set wallpaper for wlroots based desktops
swaybg -m stretch -i "${HOME}/.config/wallpapers/${WALLPAPER}" &

# If using mako notification daemon, restart it in order to update pywal theme
pkill mako && mako &

# Pywal for discord
# pywal-discord -p $HOME/.config/Lightcord_BD/themes/
